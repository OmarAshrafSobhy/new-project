package Pac;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;


public class Main {
	public static void log (String Data , String Filename )
	{
		String time_stamp=new SimpleDateFormat("yyyy.MM.dd, HH:mm:ss").format(new java.util.Date());
		System.out.println(Data+" ,["+time_stamp+"]");
		if(Filename.trim().equals(""))
			return;
	
		BufferedWriter writer = null;
		try
		{
			File logFile = new File(Filename);
			writer = new BufferedWriter(new FileWriter(logFile, true));
			writer.write(Data+"   ["+time_stamp+"]"+"\r\n");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally {
			try 
			{
				writer.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}


	public static void main(String[] args)
	{
		log("Hi It works","log.txt");
		log("Hi It works","log.txt");
		log("Hi It works","log.txt");log("Hi It works","log.txt");log("Hi It works","log.txt");
	}
}